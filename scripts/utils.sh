#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"

LOG_PREFIX="├ LOG"

function utils:log {
    echo "$LOG_PREFIX: $(date '+%Y-%m-%d-%I:%M:%S') $@"
}

function utils:log-wrap {
    local curr_prefix=$(utils:log "")
    utils:log '  ╭──────';
    sed "s/^/${curr_prefix}  │ /g";
    utils:log '  ╰──────'
}

function utils:log-command {
    utils:log "$@:"
    ("$@" 2>&1 ; echo "[[status: $?]]") | utils:log-wrap
}

function utils:env-info {
    utils:log-command nproc
    utils:log-command free -h
    utils:log-command java --version
    utils:log-command javac --version
}
